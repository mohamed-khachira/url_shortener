<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\UrlShorten;
use App\Service\TokenizedUrlGeneratorService;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Security;

class UrlUserSubscriber implements EventSubscriberInterface
{

    private $security;
    private $tokenizedUrlGeneratorService;

    public function __construct(Security $security, TokenizedUrlGeneratorService $tokenizedUrlGeneratorService)
    {
        $this->security = $security;
        $this->tokenizedUrlGeneratorService = $tokenizedUrlGeneratorService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserForUrl', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setUserForUrl(ViewEvent $event)
    {
        $url_shorten = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($url_shorten instanceof UrlShorten && $method === "POST") {
            $user = $this->security->getUser();
            if ($user) {
                $url_shorten->setUser($user);
                $url_shorten->setShortUrl($this->tokenizedUrlGeneratorService->getRandomToken());
            }
        }
    }
}

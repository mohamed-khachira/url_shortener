<?php

namespace App\Repository;

use App\Entity\UrlShorten;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UrlShorten|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlShorten|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlShorten[]    findAll()
 * @method UrlShorten[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlShortenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UrlShorten::class);
    }

    // /**
    //  * @return UrlShorten[] Returns an array of UrlShorten objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UrlShorten
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

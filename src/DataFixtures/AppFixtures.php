<?php

namespace App\DataFixtures;

use App\Entity\UrlShorten;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//use Faker\Factory;

class AppFixtures extends Fixture
{
    const DEFAULT_USER = ['email' => 'default-user@test.de', 'password' => 'default-password'];
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        //$fake = Factory::create();
        $defaultUser = new User();
        $passHash = $this->encoder->encodePassword($defaultUser, self::DEFAULT_USER['password']);

        $defaultUser->setEmail(self::DEFAULT_USER['email'])
            ->setPassword($passHash);

        $manager->persist($defaultUser);

        $urlShorten = (new UrlShorten())->setUser($defaultUser)
            ->setFullUrl("https://retentionx.com/dashboard")
            ->setShortUrl("3xYh6ng");

        $manager->persist($urlShorten);

        $manager->flush();
    }
}

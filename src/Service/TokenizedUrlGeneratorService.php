<?php

namespace App\Service;

use App\Repository\UrlShortenRepository;

class TokenizedUrlGeneratorService
{
    const length = 7;
    const CHARACTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    private $urlShortenRepository;

    public function __construct(UrlShortenRepository $urlShortenRepository)
    {
        $this->urlShortenRepository = $urlShortenRepository;
    }

    public function getRandomToken()
    {
        do {
            $token = $this->generateRandomString();
            $urlInDatabase = $this->urlShortenRepository->findOneBy([
                'shortUrl' => $token,
            ]);
        } while ($urlInDatabase);
        return $token;
    }

    public function generateRandomString()
    {
        $characters = self::CHARACTERS;
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < self::length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

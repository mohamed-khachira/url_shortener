up:
	docker-compose up -d --build

stop:
	docker-compose stop

php:
	docker-compose exec php74-service sh

mysql:
	docker-compose exec mysql8-service sh

rebuild:
	docker-compose up -d --build --force-recreate

logs:
	docker-compose logs -f
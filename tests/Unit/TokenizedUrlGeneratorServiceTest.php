<?php

namespace App\Tests\Unit;

use App\Repository\UrlShortenRepository;
use App\Service\TokenizedUrlGeneratorService;
use PHPUnit\Framework\TestCase;

class TokenizedUrlGeneratorServiceTest extends TestCase
{
    private $service;
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getMockBuilder(UrlShortenRepository::class)->disableOriginalConstructor()->getMock();
        $this->service = new TokenizedUrlGeneratorService($this->repository);
    }

    public function testGenerateRandomString(): void
    {
        $randomString = $this->service->generateRandomString();
        $this->assertIsString(
            $randomString,
            "Actual value is not a string !!"
        );
        $this->assertEquals(strlen(($randomString)), TokenizedUrlGeneratorService::length);
        $this->assertMatchesRegularExpression('/^[a-z0-9]+$/i', $randomString);
    }
}

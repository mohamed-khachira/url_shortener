<?php

namespace App\Tests\Unit;

use App\Entity\UrlShorten;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UrlShortenTest extends TestCase
{
    private UrlShorten $urlShorten;

    protected function setUp(): void
    {
        parent::setUp();

        $this->urlShorten = new UrlShorten();
    }

    public function testGetFullUrl(): void
    {
        $full_url = 'https://retentionx.com/dashboard';

        $response = $this->urlShorten->setFullUrl($full_url);

        self::assertInstanceOf(UrlShorten::class, $response);
        self::assertEquals($full_url, $this->urlShorten->getFullUrl());
    }

    public function testGetShortUrl(): void
    {
        $short_url = 'http://localhost/3xYh6ng';

        $response = $this->urlShorten->setShortUrl($short_url);

        self::assertInstanceOf(UrlShorten::class, $response);
        self::assertEquals($short_url, $this->urlShorten->getShortUrl());
    }

    public function testGetClicks(): void
    {
        $clicks = 5;

        $response = $this->urlShorten->setClicks($clicks);

        self::assertInstanceOf(UrlShorten::class, $response);
        self::assertEquals($clicks, $this->urlShorten->getClicks());
    }

    public function testGetCreatedAt(): void
    {
        $createdAt = new \DateTimeImmutable();

        $response = $this->urlShorten->setCreatedAt($createdAt);

        self::assertInstanceOf(UrlShorten::class, $response);
        self::assertEquals($createdAt, $this->urlShorten->getCreatedAt());
    }

    public function testGetUser(): void
    {
        $user = new User();

        $response = $this->urlShorten->setUser($user);

        self::assertInstanceOf(UrlShorten::class, $response);
        self::assertInstanceOf(User::class, $this->urlShorten->getUser());
    }
}
